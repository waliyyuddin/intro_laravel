<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/', 'HomeController@home');

// Route::get('/test/{nama}', function ($nama) {
//     return "Hallo all! $nama";
// });

Route::get('/register', 'AuthController@register');

Route::get('/selamat_datang', 'AuthController@welcome');
Route::post('/selamat_datang', 'AuthController@welcome_post');

Route::get('/table', function() {
    return view('data.table');
});

Route::get('/data-tables', function() {
    return view('data.data-tables');
});

//menggunakan query builder
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

//menggunakan eloquent (orm)
Route::resource('gas', 'GasController');
Route::resource('cast', 'CastController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('film', 'FilmController');
