<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    
    public function welcome(request $request){
        $nama = $request['namadepan'];
        $namablkg = $request['namabelakang'];
        

        return view('selamat_datang', compact('nama','namablkg'));
    }

    public function welcome_post(request $request){
        $nama = $request['namadepan'];
        $namablkg = $request['namabelakang'];
        

        return view('selamat_datang', compact('nama','namablkg'));
    }
}
