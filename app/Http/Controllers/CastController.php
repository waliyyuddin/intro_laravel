<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cast;
use Auth;

class CastController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    public function index() {
        //menggunakan query builder
        // $cast = DB::table('cast')->get();

        // dd($cast);
        
        //menggunakan eloquent (orm)
        $cast = cast::all();
        return view('cast.index', compact('cast'));
    }
    
    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //{menggunakan query builder}

        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);
        
        //{menggunakan eloquent (orm)}

        // $cast = new cast;
        // $cast->nama = $request["nama"];
        // $cast->umur = $request["umur"];
        // $cast->bio = $request["bio"];
        // $cast->save();

        $cast = cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
            // "user_id" => Auth::id()  //jika ada column user_id, genre_id, dan lain lain
        ]);

        return redirect('/cast');//->with('succes', 'Data berhasil disimpan!')
    }

    public function show($id) {
        //{menggunakan query builder}
        // $cast = DB::table('cast')->where('id', $id)->first();
        
        //{menggunakan eloquent (orm)}
        $cast = cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        //{menggunakan query builder}
        // $cast = DB::table('cast')->where('id', $id)->first();

        //{menggunakan eloquent (orm)}
        $cast = cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        // $request->validate([
        //     'nama' => 'required|unique:cast',
        //     'umur' => 'required',
        //     'bio' => 'required',
        // ]);

        //{menggunakan query builder}

        // $query = DB::table('cast')->where('id', $id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'umur' => $request["umur"],
        //         'bio' => $request["bio"]
        //     ]);

        //{menggunakan eloquent (orm)}

        $update = cast::where('id', $id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        //{menggunakan query builder}

        // $query = DB::table('cast')->where('id', $id)->delete();

        
        //{menggunakan eloquent (orm)}

        // $deletedRows = cast::where('id', $id)->delete(); //opsi 1
        cast::destroy($id); //opsi 2
        return redirect('/cast');
    }
}
