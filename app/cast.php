<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\cast;

class cast extends Model
{
    protected $table = "cast";

    //pake fillable (yg di masukin berarti colom yg terdapat di white list)
    protected $fillable = ["nama", "umur", "bio"];

    //pake guarded (jika ada yg di masukan, berarti termasuk k dalam black list)
    // protected $guarded = [];

    public function peran() {
        $this->hasMany('App\Peran');
    }
}
