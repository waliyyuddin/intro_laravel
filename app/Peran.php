<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $tabel = "peran";
    protected $guarded = [];

    public function film() {
        $this->belongsTo('App\film');
    }

    public function cast() {
        $this->belongsTo('App\cast');
    }
}
