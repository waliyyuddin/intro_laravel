<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = "film";
    protected $fillable = ['judul','ringkasan','tahun','poster'];

    public function genre() {
        $this->belongsToz('App\Genre');
    }

    public function peran() {
        $this->hasOne('App\Peran');
    }

    public function komentar() {
        $this->hasOne('App\Komentar');
    }
}
