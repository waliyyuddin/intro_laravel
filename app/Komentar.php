<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $tabel = "komentar";
    protected $guarded = [];

    public function user() {
        $this->belongsTo('App\User');
    }

    public function film() {
        $this->belongsTo('App\film');
    }
}
