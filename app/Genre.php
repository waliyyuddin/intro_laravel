<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $tabel = "genre";
    protected $guarded = [];

    public function film() {
        $this->hasMany('App\film');
    }
}
