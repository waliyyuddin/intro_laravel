@extends('adminlte.master')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2>Data Pemain Film</h2>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<div class="card">
    <div class="card-body">
                  <div class="card-header">
                    <h3 class="card-title">Data Pemain Film</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <a href="/cast/create" class="btn btn-primary my-3">Tambah</a>
                  <table class="table">
                      <thead class="thead-light">
                          <tr>
                              <th scope="col">#</th>
                              <th scope="col">Nama</th>
                              <th scope="col">Umur</th>
                              <th scope="col">Bio</th>
                              <th scope="col">Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($cast as $key=>$value)
                          <tr>
                              <td>{{$key + 1}}</th>
                              <td>{{$value->nama}}</td>
                              <td>{{$value->umur}}</td>
                              <td>{{$value->bio}}</td>
                              <td>
                                  <form action="/cast/{{$value->id}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                                  <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                  <input type="submit" class="btn btn-danger my-1" value="Delete">
                              </form>
                              </td>
                          </tr>
                          @empty
                          <tr colspan="3">
                              <td>No data</td>
                          </tr>  
                          @endforelse              
                      </tbody>
                  </table>
              </div>
          </div>
    </div>
</div>
@endsection