@extends('adminlte.master')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h2>Show Cast {{$cast->id}}</h2>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<div class="card">
        <div class="card-body">
                  <div class="card-header">
                    <h2 class="card-title">Show Post {{$cast->id}}</h2>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  
                <h4>Nama : {{$cast->nama}}</h4>
                <h4>Umur : {{$cast->umur}}</h4>
                <h4>Bio : {{$cast->bio}}</h4>
        </div>
</div>


@endsection