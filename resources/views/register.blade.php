<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <form action="/selamat_datang" method="POST">
        @csrf
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>
        <label for="FN"> First Name: </label> <br> <br>
        <input type="text" name="namadepan" id="FN"> <br> <br>
        <label for="LN"> Last Name: </label> <br> <br>
        <input type="text" name="namabelakang" id="LN"> <br> <br>
        <label> Gender: </label> <br> <br>
        <input type="radio" name="Gender" id="m"><label for="m">Male</label> <br>
        <input type="radio" name="Gender" id="f"><label for="f">Female</label> <br>
        <input type="radio" name="Gender" id="o"><label for="o">Other</label> <br> <br>
        <label> Nationality: </label>
        <select name="NT">
            <option value="indonesian"> Indonesian </option>
            <option value="malaysian"> Malaysian </option>
            <option value="australian"> Australian </option>
            <option value="american"> American </option>
        </select> <br> <br>
        <label> Language Spoken: </label> <br> <br>
        <input type="checkbox" id="bi"> <label for="bi">Bahasa Indonesia</label> <br>
        <input type="checkbox" id="e"> <label for="e">English</label> <br>
        <input type="checkbox" id="ot"> <label for="ot">Other</label> <br> <br>
        <label> Bio: </label> <br> <br>
        <textarea name="text" cols="35" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>