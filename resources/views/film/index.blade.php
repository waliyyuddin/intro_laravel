@extends('adminlte.master')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2>Nama Film</h2>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<div class="card">
    <div class="card-body">
                  <div class="card-header">
                    <h3 class="card-title">Nama Film</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <a href="/film/create" class="btn btn-success my-3">Tambah</a>
                  <table class="table">
                      <thead class="thead-light">
                          <tr>
                              <th scope="col">#</th>
                              <th scope="col">Judul</th>
                              <th scope="col">Ringkasan</th>
                              <th scope="col">Tahun</th>
                              <th scope="col">Poster</th>
                              <th scope="col">Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($film as $key=>$value)
                          <tr>
                              <td>{{$key + 1}}</th>
                              <td>{{$value->judul}}</td>
                              <td>{{$value->ringkasan}}</td>
                              <td>{{$value->tahun}}</td>
                              <td>{{$value->poster}}</td>
                              <td>
                                  <form action="/film/{{$value->id}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                                  <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                  <input type="submit" class="btn btn-danger my-1" value="Delete">
                              </form>
                              </td>
                          </tr>
                          @empty
                          <tr colspan="3">
                              <td>No data</td>
                          </tr>  
                          @endforelse              
                      </tbody>
                  </table>
              </div>
          </div>
    </div>
</div>
@endsection