@extends('adminlte.master')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2>Tambah Film</h2>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<div class="card">
        <div class="card-body">
                  <div class="card-header">
                    <h3 class="card-title">Tambah Film</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
            <div>
                
                    <form action="/film" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="judul">Judul</label>
                            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
                            @error('judul')
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ $message }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="ringkas">Ringkasan</label>
                            <input type="number" class="form-control" name="ringkasan" id="ringkas" placeholder="Masukkan Ringkasan">
                            @error('ringkasan')
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ $message }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun</label><br>
                            <textarea name="tahun" id="tahun" cols="150" rows="10" placeholder="Masukkan Tahun"></textarea>
                            @error('tahun')
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ $message }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="poster">Poster</label><br>
                            <textarea name="Poster" id="poster" cols="150" rows="10" placeholder="Masukkan Tahun"></textarea>
                            @error('poster')
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ $message }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </form>
            </div>
        </div>
</div>
@endsection